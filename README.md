# DC-Phoenix Dreamcast Game Services

The DC-Phoenix project is an open-source reimplementation of Dreamcast online
services which restore the online components of Dreamcast games.

This code is currently a work in progress but includes:

 - A working (but incomplete) implementation of the DreamArena authentication server
 - A partial implementation of a Starlancer game server
 
The servers all share a common codebase but generate individual binaries for
each server. 

Future goals include:

 - Finishing DreamArena authentication and the Starlancer server
 - Implementing a new open-source Toy Racer server
 
Development currently targets Linux, but there's no reason that these servers
can't be ported to other platforms.

# DreamArena Server

The DreamArena server currently works with Toy Racer only. It currently treats
each connection as a new unregistered user, but I intend to implement database backed
user registrations to make this behave just as the original server would have.

The DreamArena server is only possible due to the work of Jonas Karlsson who successfully
reverse engineered the protocol and documented it on the [Dreamcast Development Wiki](http://dreamcast.wikidot.com/dreamarena).