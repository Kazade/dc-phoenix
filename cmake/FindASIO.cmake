FIND_PATH(ASIO_INCLUDE_DIR
  NAMES
    asio.hpp
  PATHS
    /usr/include
    /usr/local/include
)

SET(ASIO_FOUND "NO")
IF(ASIO_INCLUDE_DIR)
    SET(ASIO_FOUND "YES")
ENDIF()
