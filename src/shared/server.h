#pragma once

#include <asio.hpp>

namespace dcr {

class Server {
public:
    static asio::io_service& io_service() {
        static asio::io_service io_service_;
        return io_service_;
    }
};

}
