#pragma once

#include <vector>
#include <set>
#include <unordered_map>
#include <kazbase/unicode.h>
#include "tcp_server.h"

namespace dcr {

class IRCServer;

class IRCSession : public TCPSession {
public:
    IRCSession(TCPServer<IRCSession>& server, TCPConnection::ptr connection);
    bool handle_data(const std::string& read_data);
    void respond_with_hostname(const std::string& response);

    const std::set<std::string>& channels() const { return member_of_channels_; }
    const std::string nick() const { return nick_.encode(); }

    bool is_channel_operator(const std::string& channel) const {
        return operator_of_channels_.count(channel);
    }
private:
    IRCServer& irc_server_;

    unicode nick_;
    unicode name_;

    void send_welcome_001() {
        respond_with_hostname(_u("001 {0} :Welcome to DC::Resurrection").format(nick_).encode());
    }

    void send_yourhost_002() {
        respond_with_hostname(_u("002 {0} :Your host is dreamarena.kazade.co.uk").format(nick_).encode());
    }

    void send_created_003() {
        respond_with_hostname(_u("003 {0} :This server was created Fri Dec 25 2015 - Merry Christmas!").format(nick_).encode());
    }

    void send_myinfo_004() {
        respond_with_hostname(_u("004 {0} :This is a homebrew IRC server written by Luke Benstead").format(nick_).encode());
    }

    void send_isupport_005() {
        respond_with_hostname(_u("005 {0} UHNAMES NAMESX SAFELIST HCN MAXCHANNELS=30 CHANLIMIT=#:30 MAXLIST=b:60,e:60,I:60 NICKLEN=30 CHANNELLEN=32 TOPICLEN=307 KICKLEN=307 AWAYLEN=307 MAXTARGETS=20 :are supported by this server").format(nick_).encode());
        respond_with_hostname(_u("005 {0} WALLCHOPS WATCH=128 WATCHOPTS=A SILENCE=15 MODES=12 CHANTYPES=# PREFIX=(qaohv)~&@%+ CHANMODES=beI,kfL,lj,psmntirRcOAQKVCuzNSMTGZ NETWORK=OnlineConsoles CASEMAPPING=ascii EXTBAN=~,qjncrRa ELIST=MNUCT STATUSMSG=~&@%+ :are supported by this server").format(nick_).encode());
        respond_with_hostname(_u("005 {0} EXCEPTS INVEX CMDS=KNOCK,MAP,DCCALLOW,USERIP,STARTTLS :are supported by this server").format(nick_).encode());
    }

    void send_created(const std::string& channel_name) {
        //FIXME: this returns time since the server started, not the channel
        respond(_u("329 {0} {1} {2}").format(nick_, channel_name, server().time_since_start()).encode());
    }

    void send_names_list(const std::string& channel_name);
    void send_topic(const std::string& channel_name);
    void handle_start() {}

    std::set<std::string> member_of_channels_;
    std::set<std::string> operator_of_channels_;
};

class IRCServer : public TCPServer<IRCSession> {
public:
    IRCServer(uint32_t port);

    std::set<std::string> channels() {
        std::set<std::string> result;

        for(auto& session: active_sessions()) {
            result.insert(session->channels().begin(), session->channels().end());
        }

        return result;
    }

    std::set<std::string> channel_members(const std::string& channel) {
        std::set<std::string> result;

        for(auto& session: active_sessions()) {
            if(session->channels().count(channel)) {
                result.insert(session->nick());
            }
        }

        return result;
    }

    std::set<std::string> channel_operators(const std::string& channel) {
        std::set<std::string> result;

        for(auto& session: active_sessions()) {
            if(session->is_channel_operator(channel)) {
                result.insert(session->nick());
            }
        }

        return result;
    }

    void set_channel_topic(const std::string& channel_name, const std::string& topic) {
        if(channel_members(channel_name).empty()) {
            // No one in this channel - don't bother
            return;
        }
        channel_topics_[channel_name] = topic;
    }

    std::string channel_topic(const std::string& channel_name) {
        if(channel_topics_.count(channel_name) == 0) {
            return "";
        }
        return channel_topics_[channel_name];
    }

private:
    std::unordered_map<std::string, std::string> channel_topics_;

};

}
