#include <set>
#include <kazbase/unicode.h>
#include "./http_server.h"

namespace dcr {

HTTPServer::HTTPServer(uint32_t port):
    TCPServer(port) {

}

HTTPServer::~HTTPServer() {

}

HTTPSession::HTTPSession(TCPServer<HTTPSession>& server, TCPConnection::ptr connection):
    TCPSession(server, connection),
    server_(server),
    http_server_(dynamic_cast<HTTPServer&>(server)) {

}

typedef std::map<std::string, std::string> HeaderDict;

void parse_request(const std::string& request, HeaderDict& headers, std::string& body) {
    const std::set<unicode> METHODS = { "GET", "POST", "HEAD", "PUT", "DELETE" };

    unicode req = request; // Make use of the unicode class

    auto lines = req.split("\n");

    auto first_line = lines.front();

    auto method_path_proto = first_line.split("\t ", -1, false);

    if(method_path_proto.size() != 3) {
        return;
    }

    auto method = method_path_proto[0].strip();
    auto path = method_path_proto[1].strip();

    headers["REQUEST_METHOD"] = method.upper().encode();
    if(path.contains("?")) {
        headers["QUERY_STRING"] = path.split("?", 1).back().encode();
    }

    headers["PATH"] = path.split("?").front().encode();

    auto proto = method_path_proto[2];

    if(METHODS.count(method) != 1) {
        return;
    }

    uint32_t i = 1;
    for(; i < lines.size(); ++i) {
        auto& line = lines[i];

        if(line.strip().empty()) {
            break;
        }
    }

    body = _u("\n").join(std::vector<unicode>(lines.begin() + i, lines.end())).encode();
}

std::string create_response(const std::string& content, uint32_t status=200) {
    return _u("HTTP/1.0 {0} {1}\r\nContent-Type: text/html\r\n\r\n{2}").format(
        status,
        "OK",
        content
    ).encode();
}

bool HTTPSession::handle_data(const std::string &read_data) {
    std::cout << read_data << std::endl;

    HeaderDict headers;
    std::string body;
    parse_request(read_data, headers, body);

    respond(create_response("<html><body>Hello World!</body></head>"));

    return true; //Terminate
}

}
