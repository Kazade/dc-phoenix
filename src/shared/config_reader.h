#ifndef CONFIG_READER_H
#define CONFIG_READER_H

#include <map>
#include <memory>
#include <stdexcept>
#include <kazbase/unicode.h>

class SectionDoesNotExist : public std::runtime_error {
public:
    SectionDoesNotExist(const std::string& what):
        std::runtime_error(what) {}
};

class KeyDoesNotExist : public std::runtime_error {
public:
    KeyDoesNotExist(const std::string& what):
        std::runtime_error(what) {}
};

namespace impl {
    template<typename T>
    struct value_caster {
        static T cast(const unicode& value) {
            return T(value);
        }
    };

    template<>
    struct value_caster<int32_t> {
        static int32_t cast(const unicode& value) {
            return std::atoi(value.encode().c_str());
        }
    };

    template<>
    struct value_caster<bool> {
        static bool cast(const unicode& value) {
            return value.lower() == "true";
        }
    };
}

class Config {
public:
    std::vector<unicode> sections() const;
    std::vector<unicode> keys(const unicode& section) const;

    template<typename T>
    T get_value(const unicode& section, const unicode& key) {
        auto section_it = sections_.find(section);
        if(section_it == sections_.end()) {
            throw SectionDoesNotExist(section.encode());
        }

        auto key_it = section_it->second.find(key);
        if(key_it == section_it->second.end()) {
            throw KeyDoesNotExist(key);
        }

        unicode value = key_it->second;

        return impl::value_caster<T>::cast(value);
    }

private:
    typedef std::map<unicode, unicode> KeyMap;
    typedef std::map<unicode, KeyMap> SectionMap;

    SectionMap sections_;

    friend std::shared_ptr<Config> loads(const unicode &filename);
};

std::shared_ptr<Config> load(const unicode& filename);
std::shared_ptr<Config> loads(const unicode& data);

#endif // CONFIG_READER_H
