#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <kazbase/unicode.h>
#include "./tcp_server.h"

namespace dcr {

class HTTPServer;

class HTTPSession : public TCPSession {
public:
    HTTPSession(TCPServer<HTTPSession>& server, TCPConnection::ptr connection);

private:
    TCPServer<HTTPSession>& server_;
    HTTPServer& http_server_;

    bool handle_data(const std::string &read_data) override;
    void handle_start() {}
};


class HTTPServer : public TCPServer<HTTPSession> {
public:
    HTTPServer(uint32_t port=80);
    ~HTTPServer();

private:

};


}

#endif // HTTP_SERVER_H
