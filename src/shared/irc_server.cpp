#include <kazbase/unicode.h>

#include "irc_server.h"

namespace dcr {

IRCServer::IRCServer(uint32_t port):
    TCPServer(port) {

}

IRCSession::IRCSession(TCPServer<IRCSession>& server, TCPConnection::ptr connection):
    TCPSession(server, connection),
    irc_server_(dynamic_cast<IRCServer&>(server)) {

}

void IRCSession::respond_with_hostname(const std::string& response) {
    std::string final = ":192.168.1.33 " + response;
    final += "\r\n";
    std::cout << "Responding: " << final << std::endl;
    connection()->socket().send(asio::buffer(final, 1024));
}

void IRCSession::send_names_list(const std::string& channel_name) {
    auto members = irc_server_.channel_members(channel_name);
    auto admins = irc_server_.channel_operators(channel_name);

    std::vector<unicode> names_list;

    for(auto nick: members) {
        if(admins.count(nick) == 0) {
            names_list.push_back(nick);
        } else {
            names_list.push_back("@" + nick);
        }
    }

    auto final_string = _u(" ").join(names_list);

    respond_with_hostname(_u("353 {0} = {1} :{2}\r\n366 {3} {4} :End of /NAMES list.").format(
        nick_, channel_name, final_string, nick_, channel_name
    ).encode());
}

void IRCSession::send_topic(const std::string &channel_name) {
    auto topic = irc_server_.channel_topic(channel_name);
    if(topic.empty()) {
        respond_with_hostname(_u("331 {0} {1} :No topic is set.").format(nick_, channel_name).encode());
    } else {
        respond(_u("332 {0} {1} :{2}").format(nick_, channel_name, topic).encode());
    }
}

bool IRCSession::handle_data(const std::string& read_data) {
    for(auto line: unicode(read_data).split("\n")) {
        unicode input_read(line);
        auto parts = input_read.split(" ");
        auto cmd = parts[0].strip();

        std::cout << line << std::endl;

        if(cmd == "USRIP") {
            /* This seems to be a starlancer specific command
             * the original server returns 0.0.0.0
             */
            respond(":s 302 :=+@0.0.0.0");
        } else if(cmd == "NICK") {
            nick_ = parts[1];
        } else if(cmd == "USER") {
            name_ = input_read.split(":")[1];
            respond("PING :11572405"); //FIXME: Find out what this number is
        } else if(cmd == "PONG") {
            send_welcome_001();
            send_yourhost_002();
            send_created_003();
            send_myinfo_004();
            send_isupport_005();
        } else if(cmd == "WHOIS") {

        } else if(cmd == "JOIN") {
            auto channel_name = parts[1].strip().encode();
            bool exists = !irc_server_.channel_members(channel_name).empty();

            member_of_channels_.insert(channel_name);
            if(!exists) {
                operator_of_channels_.insert(channel_name);
            }

            send_names_list(channel_name);
        } else if(cmd == "NAMES") {
            auto channel_name = parts[1].strip().encode();
            send_names_list(channel_name);
        } else if(cmd == "TOPIC") {
            auto channel_name = parts[1].strip().encode();
            if(parts.size() > 2) {
                // If we were sent a topic, change it!
                irc_server_.set_channel_topic(channel_name, parts[2].strip().encode());
            } else {
                send_topic(channel_name);
            }
        } else if(cmd == "QUIT") {
            return true;
        }
    }

    start_read();

    return false;
}

}
