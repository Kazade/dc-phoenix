#include <iostream>
#include "tcp_server.h"

namespace dcr {

TCPSession::TCPSession(SessionServer& server, TCPConnection::ptr connection):
    server_(server),
    connection_(connection) {

}

void TCPSession::start() {
    handle_start();
    start_read();
}

void TCPSession::start_read() {
    auto& socket = connection_->socket();

    socket.async_read_some(
        asio::buffer(read_buffer_, READ_BUFFER_SIZE),
        std::bind(&TCPSession::handle_read, this,
            std::placeholders::_1,
            std::placeholders::_2
        )
    );
}

void TCPSession::handle_read(const asio::error_code& error, std::size_t bytes_transferred) {
    bool terminate = false;
    if(bytes_transferred) {
        std::string read_data(read_buffer_, read_buffer_ + bytes_transferred);
        terminate = handle_data(read_data);
    }

    if(terminate || error == asio::error::connection_reset) {
        std::cerr << "Terminating connection" << std::endl;
        finish();
        return;
    }

    start_read();
}

void TCPSession::respond(const std::string& response) {
    connection_->socket().write_some(asio::buffer(response, 1024));
}


}
