
#include "config_reader.h"

std::shared_ptr<Config> loads(const unicode &data) {
    auto lines = data.split("\n", -1, false);

    unicode section = "";

    auto ret = std::make_shared<Config>();

    for(auto& line: lines) {
        line = line.strip();
        if(line.starts_with("[") && line.ends_with("]")) {
            section = line.slice(1, -1);
        } else if(line.count("=")) {
            auto key_value = line.split("=");

            auto key = key_value[0].strip();
            auto value = key_value[1].strip();

            ret->sections_[section][key] = value;
        }
    }

    return ret;
}
