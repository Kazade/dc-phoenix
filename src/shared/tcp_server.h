#pragma once

#include <iostream>
#include <asio.hpp>
#include <cstdint>
#include <memory>
#include <chrono>

#include "server.h"

namespace dcr {


class TCPConnection {
public:
    typedef std::shared_ptr<TCPConnection> ptr;
    TCPConnection(asio::io_service& service):
        socket_(service) {}

    ~TCPConnection() {
        if(socket_.is_open()) {
            socket_.close();
        }
    }

    asio::ip::tcp::socket& socket() { return socket_; }
    const asio::ip::tcp::socket& socket() const { return socket_; }

    std::string remote_ip() const {
        return socket().remote_endpoint().address().to_string();
    }

private:
    asio::ip::tcp::socket socket_;
};


class BaseSession {
public:
    virtual ~BaseSession() {}
};

class SessionServer {
public:
    virtual ~SessionServer() {}
    virtual void remove_active_session(BaseSession* session) = 0;
    virtual uint64_t time_since_start() = 0;
};

class TCPSession : public BaseSession {
public:
    TCPSession(SessionServer& server, TCPConnection::ptr connection);

    void start();
    void finish() {
        std::cout << "Ending session with " << connection_->remote_ip() << std::endl;
        server_.remove_active_session(this);
    }

    void start_read();
    void handle_read(const asio::error_code& error, std::size_t bytes_transferred);
    void respond(const std::string& response);

protected:
    TCPConnection::ptr connection() { return connection_; }
    SessionServer& server() { return server_; }

private:
    SessionServer& server_;

    virtual void handle_start() = 0;
    virtual bool handle_data(const std::string& read_data) = 0; //return True to terminate

    TCPConnection::ptr connection_;

    enum { READ_BUFFER_SIZE = 1024 };
    char read_buffer_[READ_BUFFER_SIZE];

};

const uint32_t MAX_CONNECTIONS = 15;

template<typename SessionType>
class TCPServer : public Server, public SessionServer {
public:
    TCPServer(uint32_t port);

    void start();

    void remove_active_session(BaseSession* session) {
        active_sessions_.erase(
            std::remove_if(
                active_sessions_.begin(),
                active_sessions_.end(), [=](std::shared_ptr<SessionType>& item) -> bool {
                    return item.get() == session;
                }),
            active_sessions_.end()
        );

        // Restart accepting connections if we removed some things
        if(!accepting_ && active_sessions_.size() < MAX_CONNECTIONS) {
            accepting_ = true;
            start_accept();
        }
    }

    const std::vector<std::shared_ptr<SessionType>>& active_sessions() const {
        return active_sessions_;
    }

    uint64_t time_since_start() {
        auto seconds = std::chrono::system_clock::now().time_since_epoch().count();
        return seconds - start_time_;
    }

private:
    asio::ip::tcp::acceptor acceptor_;
    bool accepting_ = true;

    void start_accept();
    void handle_accept(TCPConnection::ptr connection, std::shared_ptr<SessionType> session, const asio::error_code& error);

    virtual void on_accept(std::shared_ptr<SessionType> session, const asio::error_code& error) {}

    std::vector<std::shared_ptr<SessionType> > active_sessions_;

    uint64_t start_time_;
};

template<typename SessionType>
TCPServer<SessionType>::TCPServer(uint32_t port):
    acceptor_(Server::io_service(), asio::ip::tcp::endpoint(asio::ip::tcp::v4(), port)) {

}

template<typename SessionType>
void TCPServer<SessionType>::start() {
    start_time_ = std::chrono::system_clock::now().time_since_epoch().count();
    start_accept();
}

template<typename SessionType>
void TCPServer<SessionType>::start_accept() {
    TCPConnection::ptr new_connection = std::make_shared<TCPConnection>(
        Server::io_service()
    );

    std::shared_ptr<SessionType> new_session = std::make_shared<SessionType>(*this, new_connection);

    acceptor_.async_accept(
        new_connection->socket(),
        std::bind(&TCPServer::handle_accept, this, new_connection, new_session, std::placeholders::_1)
    );
}

template<typename SessionType>
void TCPServer<SessionType>::handle_accept(TCPConnection::ptr connection, std::shared_ptr<SessionType> session, const asio::error_code& error) {
    if(!error) {
        std::cout << "Received connection from " << connection->remote_ip() << std::endl;
        on_accept(session, error);
        active_sessions_.push_back(session);
        active_sessions_.back()->start();

        std::cout << active_sessions_.size() << " active sessions" << std::endl;
    } else {
        std::cout << "Error during accept" << std::endl;
    }

    if(active_sessions_.size() < MAX_CONNECTIONS) {
        start_accept(); // Start listening for more connections
    } else {
        accepting_ = false;
    }
}

}
