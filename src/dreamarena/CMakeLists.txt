
SET(sources
    main.cpp
    dreamarena_server.cpp
    user.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/irc_server.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/tcp_server.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/http_server.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/config_reader.cpp
)

ADD_EXECUTABLE(dreamarena_server ${sources})

CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/services/dreamarena.service ${CMAKE_CURRENT_BINARY_DIR}/dreamarena.service)
INSTALL(TARGETS dreamarena_server RUNTIME DESTINATION bin)
INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/dreamarena.service DESTINATION /etc/systemd/system)
