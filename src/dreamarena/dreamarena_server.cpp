
#include <kazbase/unicode.h>

#include "dreamarena_server.h"
#include "./user.h"

const std::string SEPARATOR = "<!KADBICHBIOPETERPAN=>";
const std::string ONLINE_GAME = "<ONLINEGAME>";
const std::string ONLINE_START = "<ONLINESTRT>";
const std::string USER_LIST_SPECIFY = "<USERLISTSP>";
const std::string USERNAME_SELECTED = "<USERNAMESL>";
const std::string GAME_NAME_ANON = "<GAMENAMNON>";
const std::string GAME_NAME_REGISTERED = "<GAMENAMREG>";
const std::string GAME_NAME_SELECTED = "<GAMENAMESL>";
const std::string OK_SERVER_AUTH = "<OKSERVAUTH>";
const std::string OK_AUTHORISED = "<OKAUTHORIS>";
const std::string USER_INFO_PK = "<USERINFOPK>";
const std::string SERVICE_CODE = "<SERVICECOD>";
const std::string SERVICE_DETAILS = "<SERVICEDET>";
const std::string SERVICE_LIST = "<SERVICELST>";
const std::string SERVICE_SAV = "<SERVICESAV>";
const std::string SERVICE_STR = "<SERVICESTR>";
const std::string SERVICE_END = "<SERVICEEND>";
const std::string AUTH_SUCCESS = "<AUTHSUCCES>";

enum ServiceType {
    SERVICE_TYPE_LOBB,
    SERVICE_TYPE_GMSV,
    SERVICE_TYPE_CHAT,
    SERVICE_TYPE_GMSP
};

std::string truncate(const std::string& str, uint32_t length) {
    return str.substr(0, length);
}

std::string to_string(ServiceType type) {
    switch(type) {
        case SERVICE_TYPE_LOBB:
            return "lobb";
        case SERVICE_TYPE_CHAT:
            return "chat";
        case SERVICE_TYPE_GMSP:
            return "gmsp";
        case SERVICE_TYPE_GMSV:
            return "gmsv";
        default:
            throw std::runtime_error("Invalid service type");
    }
}

ServiceType to_service_type(const std::string name) {
    if(name == "lobb") {
        return SERVICE_TYPE_LOBB;
    } else if(name == "chat") {
        return SERVICE_TYPE_CHAT;
    } else if(name == "gmsp") {
        return SERVICE_TYPE_GMSP;
    } else if(name == "gmsv") {
        return SERVICE_TYPE_GMSV;
    } else {
        throw std::runtime_error("Invalid service name");
    }
}

struct Service {
    ServiceType type;
    std::string description;
    std::string ipv4;
    uint16_t port;

    Service(ServiceType type, const std::string& desc, const std::string& ipv4, uint16_t port):
        type(type), description(desc), ipv4(ipv4), port(port) {}
};

struct Game {
    std::vector<Service> services;

    int service_count(ServiceType type) {
        return std::count_if(services.begin(), services.end(), [=](Service& serv) -> bool { return serv.type == type; } );
    }
};


static std::map<std::string, std::shared_ptr<Game>> games;
static bool games_initialized = false;

void init_games() {
    if(games_initialized) return;

    auto toyracer = std::make_shared<Game>();
    toyracer->services.push_back(Service(SERVICE_TYPE_LOBB, "Dreamcast Live", "198.133.10.182", 2048));
    games["toyracer"] = toyracer;

    games_initialized = true;
}

namespace dcr {

std::string build_message(const std::vector<std::string>& parts) {
    std::string result = SEPARATOR;
    for(auto& part: parts) {
        result += part;
        result += SEPARATOR;
    }
    result += "\r\n";
    return result;
}

std::vector<std::string> split_message(const std::string& string) {
    std::vector<std::string> ret;
    size_t pos = 0;
    std::string s = string;
    while ((pos = s.find(SEPARATOR)) != std::string::npos) {
        auto token = unicode(s.substr(0, pos)).strip().encode();

        if(!token.empty()) {
            ret.push_back(token);
        }
        s.erase(0, pos + SEPARATOR.length());
    }
    return ret;
}

std::string padded_number(int32_t value) {
    std::string ret = std::to_string(value);
    while(ret.length() < 4) {
        ret = "0" + ret;
    }
    return ret;
}

DreamArenaSession::DreamArenaSession(TCPServer<DreamArenaSession>& server, TCPConnection::ptr connection):
    TCPSession(server, connection),
    dreamarena_server_(dynamic_cast<DreamArenaServer&>(server))  {

}

void DreamArenaSession::handle_start() {
    init_games();

    std::cout << "Started session for " << connection()->remote_ip() << std::endl;
    respond(build_message({ONLINE_GAME}));
}


bool DreamArenaSession::handle_data(const std::string& read_data) {
    const uint32_t MAX_CONSOLE_ID_LENGTH = 30;
    const std::map<std::string, uint32_t> EXPECTED_ARG_COUNT = {
        { ONLINE_START, 3 },
        { USERNAME_SELECTED, 4 },
        { GAME_NAME_SELECTED, 2 },
        { OK_AUTHORISED, 1 },
        { SERVICE_CODE, 2 },
        { SERVICE_LIST, 2 },
        { AUTH_SUCCESS, 1 }
    };

    bool terminate = false;

    auto parts = split_message(read_data);
    if(parts.empty()) {
        std::cerr << "Ill-formed command. Terminating" << std::endl;
        return true;
    }

    auto command = parts[0];

    if(!EXPECTED_ARG_COUNT.count(command)) {
        std::cerr << "Unexpected command: " << command << std::endl;
        terminate = true;
    } else if(parts.size() != EXPECTED_ARG_COUNT.find(command)->second) {
        std::cerr << "Unexpected number of arguments for command: " << command << std::endl;
        std::cerr << "Data was: '" << read_data << "'" << std::endl;
        terminate = true;
    } else {
        if(command == ONLINE_START) {
            console_id_ = truncate(parts[2], MAX_CONSOLE_ID_LENGTH);

            if(console_id_.empty()) {
                std::cerr << "Invalid console ID specified" << std::endl;
                terminate = true;
            } else {
                respond(build_message({USER_LIST_SPECIFY}));
            }
        } else if(command == USERNAME_SELECTED) {
            auto game_name = parts[3];
            if(!games.count(game_name)) {
                std::cerr << "Invalid game: " << game_name << std::endl;
                terminate = true;
            } else {
                auto entered_gamer_name = parts[2];

                game_name_ = game_name;

                auto existing_users = User::query(&User::gamer_name, entered_gamer_name);
                if(!existing_users.empty() && existing_users.front().console_id != console_id_) {
                    /*
                     * The username was already registered, they'll need to pick another one!
                     */
                    respond(build_message({
                        {GAME_NAME_REGISTERED},
                        entered_gamer_name
                    }));
                } else {
                    /*
                     * Store the console <> username mapping and return that it was OK
                     */
                    std::cout << "Stored new user: " << entered_gamer_name << std::endl;
                    auto user = User::get_or_create(console_id_);
                    user.gamer_name = entered_gamer_name;
                    User::save(user);

                    respond(build_message({GAME_NAME_ANON}));
                }
            }
        } else if(command == GAME_NAME_SELECTED) {
            respond(build_message({OK_SERVER_AUTH}));
        } else if(command == OK_AUTHORISED) {
            auto user = User::get_or_create(console_id_);

            std::string user_id = std::to_string(user.pk);
            std::string user_name = user.gamer_name;
            std::string language = "en";
            std::string country = "uk";
            std::string age = "18";
            std::string chat_name = user.gamer_name;
            std::string gamer_name = user.gamer_name;
            std::string email = user.gamer_name + "@example.com";

            respond(build_message(
                {
                    USER_INFO_PK,
                    user_id,
                    user_name,
                    language,
                    country,
                    age,
                    chat_name,
                    gamer_name,
                    email
                }
            ));

        } else if(command == SERVICE_CODE) {
            auto game = games[game_name_];
            auto service = parts[1];
            auto service_type = to_service_type(service);
            service_type_ = to_string(service_type);
            respond(build_message({
                SERVICE_DETAILS,
                padded_number(game->service_count(service_type))
            }));
        } else if(command == SERVICE_LIST) {
            auto game = games[game_name_];
            auto service_number = (uint32_t) std::atoi(parts[1].c_str()) - 1;

            if(service_number >= game->services.size()) {
                service_number = 0;
                parts[1] = padded_number(service_number);
            }
            auto& service = game->services.at(service_number);

            respond(build_message({
                SERVICE_SAV,
                SERVICE_STR + parts[1],
                service_type_,
                service.description,
                service.ipv4,
                std::to_string(service.port),
                SERVICE_END
            }));
        } else if(command == AUTH_SUCCESS) {
                // We are done!
            return true;
        }
    }

    return terminate;
}

}
