#include <stdexcept>

#include "./user.h"

static std::vector<User> users;

User User::create(const std::string& console_id) {
    static uint32_t id = 0;

    User new_user;
    new_user.pk = ++id;
    new_user.console_id = console_id;
    users.push_back(new_user);
    return new_user;
}

User User::get_or_create(const std::string &console_id) {
    auto users = query(&User::console_id, console_id);
    if(users.empty()) {
        return User::create(console_id);
    } else {
        return users.front();
    }
}

void User::save(const User& user) {
    uint32_t i = 0;
    int32_t to_replace = -1;

    for(auto& existing: users) {
        if(existing.pk == user.pk) {
            to_replace = i;
            break;
        }
        ++i;
    }

    if(to_replace == -1) {
        throw std::runtime_error("Couldn't find expected user");
    } else {
        users[to_replace] = user;
    }
}

std::vector<User> User::query(uint32_t User::* field, uint32_t value) {
    std::vector<User> ret;

    for(auto& user: users) {
        if(user.*field == value) {
            ret.push_back(user);
        }
    }

    return ret;
}

std::vector<User> User::query(std::string User::* field, const std::string& value) {
    std::vector<User> ret;
    for(auto& user: users) {
        if(user.*field == value) {
            ret.push_back(user);
        }
    }
    return ret;
}

