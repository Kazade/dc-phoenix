#pragma once

#include "../shared/tcp_server.h"

namespace dcr {

class DreamArenaServer;

class DreamArenaSession : public TCPSession {
public:
    DreamArenaSession(TCPServer<DreamArenaSession>& server, TCPConnection::ptr connection);

private:
    DreamArenaServer& dreamarena_server_;

    enum { READ_BUFFER_SIZE = 1024 };
    char read_buffer_[READ_BUFFER_SIZE];

    std::string console_id_;
    std::string game_name_;
    std::string service_type_;

    void handle_start() override;
    bool handle_data(const std::string &read_data) override;
};

class DreamArenaServer : public TCPServer<DreamArenaSession> {
public:
    DreamArenaServer(uint32_t port):
        TCPServer(port) {

    }
};

}
