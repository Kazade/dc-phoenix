
#include <csignal>
#include <cstdlib>
#include <iostream>

#include "./dreamarena_server.h"
#include "./registration_server.h"

static bool running = true;

void sigint_handler(int s) {
    running = false;
}

int main(int argc, char* argv[]) {
    struct sigaction si_handler;

    // Attach to sigint
    si_handler.sa_handler = sigint_handler;
    sigemptyset(&si_handler.sa_mask);
    si_handler.sa_flags = 0;
    sigaction(SIGINT, &si_handler, NULL);

    std::cout << "======================================" << std::endl;
    std::cout << "DC::Phoenix DreamArena Server 0.1" << std::endl;
    std::cout << "======================================" << std::endl << std::endl;

    dcr::DreamArenaServer server(8010);
    dcr::HTTPServer reg_server(80);

    std::cout << "Starting the TCP server" << std::endl;
    server.start();

    std::cout << "Starting the registration server" << std::endl;
    reg_server.start();

    while(running) {
        // Run the IO service until we are done
        dcr::Server::io_service().run_one();
    }

    std::cout << "Shutting down the server" << std::endl;

    return 0;
}
