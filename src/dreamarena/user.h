#ifndef USER_H
#define USER_H

#include <vector>
#include <string>
#include <cstdint>

struct User {
    uint32_t pk;
    std::string console_id;
    std::string short_name;
    std::string user_id;
    std::string preferred_language;
    std::string country_id;
    uint32_t age;
    std::string chat_name;
    std::string gamer_name;
    std::string email_address;

    static User create(const std::string& console_id);
    static void save(const User& user);
    static std::vector<User> query(uint32_t User::* field, uint32_t value);
    static std::vector<User> query(std::string User::* field, const std::string& value);
    static User get_or_create(const std::string& console_id);
};


#endif // USER_H
