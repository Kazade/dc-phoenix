#include <csignal>
#include <cstdlib>
#include <iostream>

#include "../shared/irc_server.h"

static bool running = true;

void sigint_handler(int s) {
    running = false;
}

int main(int argc, char* argv[]) {
    struct sigaction si_handler;

    // Attach to sigint
    si_handler.sa_handler = sigint_handler;
    sigemptyset(&si_handler.sa_mask);
    si_handler.sa_flags = 0;
    sigaction(SIGINT, &si_handler, NULL);

    std::cout << "======================================" << std::endl;
    std::cout << "DC::Phoenix Starlancer Server 0.1" << std::endl;
    std::cout << "======================================" << std::endl << std::endl;

    std::cout << "Starting the IRC server" << std::endl;
    dcr::IRCServer irc_server(6667);
    irc_server.start();

    while(running) {
        // Run the IO service until we are done
        dcr::Server::io_service().run_one();
    }

    std::cout << "Shutting down the server" << std::endl;

    return 0;
}
